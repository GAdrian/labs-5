﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;


namespace Kontek
{
    public class Kontener : IContainer
    {
        Dictionary<Type, Type> typs = new Dictionary<Type, Type>();
        Dictionary<Type, Object> slownik2 = new Dictionary<Type, Object>();

        public void Register(System.Reflection.Assembly assembly)
        {
            Type[] typy = assembly.GetTypes();

            foreach (Type typ in typy)
            {
                if (!typ.IsNotPublic)
                    this.Register(typ);
            }
        }

        public void Register(Type type)
        {
            Type[] interfejsy = type.GetInterfaces();
            foreach (Type item in interfejsy)
            {
                if (typs.ContainsKey(item))
                    typs[item] = type;
                else
                    typs.Add(type, item);
            }
        }

        public void Register<T>(T impl) where T : class
        {
            //typs.Add(typeof(T), null);
            Type[] interfejsy = impl.GetType().GetInterfaces();
            foreach (Type item in interfejsy)
            {
                if (slownik2.ContainsKey(item))
                    slownik2[item] = impl;
                else
                    slownik2.Add(item, impl);
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            this.Register(provider.Invoke() as T);
        }

        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            if (slownik2.ContainsKey(type))
            {
                return slownik2[type];
            }

            if (!typs.ContainsKey(type))
            {
                return null;
            }
            else
            {
                ConstructorInfo[] konstruktory = typs[type].GetConstructors();

                foreach (ConstructorInfo konstruktor in konstruktory)
                {
                    ParameterInfo[] parametry = konstruktor.GetParameters();

                    if (parametry.Length == 0)
                    {
                        return Activator.CreateInstance(typs[type]);////
                    }
                    else
                    {
                        List<object> listaParametrów = new List<object>();

                        foreach (ParameterInfo parametr in parametry)
                        {
                            object obiekt = this.Resolve(parametr.GetType());

                            if (obiekt == null)
                            {
                                throw new PK.Container.UnresolvedDependenciesException();
                            }

                            listaParametrów.Add(obiekt);
                        }

                        return Activator.CreateInstance(type, listaParametrów);
                    }
                }

                return null;
            }
        }
    }

}