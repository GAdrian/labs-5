﻿using System;
using System.Reflection;
using PK.Container;
namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Kontener);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(void));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(void));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(void));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(void));

        #endregion
    }
}
